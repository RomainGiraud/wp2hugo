package main

//package gitlab.com/RomainGiraud/WpExport

import (
	"crypto/tls"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/beevik/etree"
	"github.com/google/uuid"
	"github.com/mozillazg/go-slugify"
	"golang.org/x/net/html"

	md "github.com/JohannesKaufmann/html-to-markdown"
)

var ImgSeparator = "|"
var AltSeparator = "'"

type Comment struct {
	id      string
	name    string
	email   string
	date    time.Time
	content string
	parent  string
}

func (comment *Comment) Export(output string) {
	filename := fmt.Sprintf("%v/comment-%v.yml", output, comment.date.Unix())
	fmt.Println("++ ", filename)
	f, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	f.WriteString("_id: \"" + comment.id + "\"\n")
	f.WriteString("_parent: \"" + comment.parent + "\"\n")
	f.WriteString("date: \"" + comment.date.Format(time.RFC3339) + "\"\n")
	f.WriteString("name: \"" + comment.name + "\"\n")
	f.WriteString("email: \"" + comment.email + "\"\n")
	f.WriteString("message: |\n  " + strings.Replace(comment.content, "\n", "\n  ", -1) + "\n")
}

type Post struct {
	title      string
	link       string
	date       time.Time
	content    string
	coverImage string
	categories []string
	tags       []string
	comments   []Comment
}

func CleanHtml(blog *Blog, imageDir string, selec *goquery.Selection) {
	IsTag := func(tag string) func(node *html.Node) bool {
		return func(node *html.Node) bool {
			return node != nil && node.Type == html.ElementNode && node.Data == tag
		}
	}

	IsLink := IsTag("a")
	IsImg := IsTag("img")
	IsFigure := IsTag("figure")

	ExtractImgId := func(classes string) string {
		id := ""
		for _, class := range strings.Split(classes, " ") {
			if strings.HasPrefix(class, "wp-image-") {
				id = class[len("wp-image-"):]
			}
		}
		return id
	}

	type Figure struct {
		link    string
		caption string
	}

	var CreateGallery func(figures []Figure, caption string) *html.Node
	CreateGallery = func(figures []Figure, caption string) *html.Node {
		attr := make([]html.Attribute, 2)
		/*
			if len(figures) == 1 && len(caption) != 0 {
				figures[0] += "|" + caption
				caption = ""
			}
		*/
		src := ""
		for _, i := range figures {
			src += i.link
			if len(i.caption) != 0 {
				src += AltSeparator + html.EscapeString(i.caption)
			}
			src += ImgSeparator
		}
		src = src[:len(src)-1]
		attr[0] = html.Attribute{
			Key: "src",
			Val: src,
		}
		attr[1] = html.Attribute{
			Key: "caption",
			Val: caption,
		}
		return &html.Node{
			Type:     html.ElementNode,
			DataAtom: 0,
			Data:     "gallery",
			Attr:     attr,
		}
	}

	var CleanSiblingLinks func(node *html.Node) []*html.Node
	CleanSiblingLinks = func(node *html.Node) []*html.Node {
		toRemove := make([]*html.Node, 0)
		figures := make([]Figure, 0)
		for child := node.FirstChild; child != nil; child = child.NextSibling {
			if IsLink(child) {
				if child.FirstChild == nil {
					// Simple link
				} else {
					if child.FirstChild.NextSibling != nil {
						panic("More than one children")
					} else {
						// A single child
						img := child.FirstChild
						if IsImg(img) {
							// Link has a single child and it is an image.
							// We have to transform it.
							uri := ""
							for _, attr := range child.Attr {
								if attr.Key == "href" {
									uri = attr.Val
								}
							}
							alt := ""
							for _, attr := range img.Attr {
								if attr.Key == "alt" && attr.Val != "" {
									alt = attr.Val
								}
							}

							figures = append(figures, Figure{link: uri, caption: alt})
							toRemove = append(toRemove, child)
							if !IsLink(child.NextSibling) {
								caption := ""
								// Single figure in same block
								if len(figures) == 1 {
									caption = figures[0].caption
									figures[0].caption = ""
								}
								child.InsertBefore(CreateGallery(figures, caption), child)
								figures = nil
							}
						} else {
							panic(fmt.Sprintf("Child is not an image (is text: %v)", img.Type == html.TextNode))
						}
					}
				}
			}
		}
		return toRemove
	}

	var ExtractFigure func(node *html.Node) ([]Figure, string)
	var ExtractFigureList func(node *html.Node) ([]Figure, string)
	var CleanFigures func(node *html.Node) []*html.Node

	ExtractFigureList = func(node *html.Node) ([]Figure, string) {
		figures := make([]Figure, 0)
		for child := node.FirstChild; child != nil; child = child.NextSibling {
			if child.Data == "li" && child.FirstChild != nil && child.FirstChild == child.LastChild && child.FirstChild.Data == "figure" {
				list, capt := ExtractFigure(child.FirstChild)
				if len(capt) != 0 {
					panic("Sub-caption exists")
				}
				figures = append(figures, list...)
			} else {
				panic("Invalid child for list")
			}
		}
		return figures, ""
	}

	ExtractFigure = func(node *html.Node) ([]Figure, string) {
		figures := make([]Figure, 0)
		caption := ""

		for child := node.FirstChild; child != nil; child = child.NextSibling {
			if child.Data == "figcaption" {
				if caption != "" {
					panic("Caption already sets")
				}
				caption = child.FirstChild.Data
			} else if child.Data == "img" {
				//src := ""
				alt := ""
				imgId := ""
				for _, attr := range child.Attr {
					if attr.Key == "alt" && attr.Val != "" {
						alt = attr.Val
					} else if attr.Key == "src" {
						//src = attr.Val
					} else if attr.Key == "class" {
						imgId = ExtractImgId(attr.Val)
					}
				}
				figures = append(figures, Figure{link: imgId, caption: alt})
			} else if child.Data == "ul" || child.Data == "ol" {
				list, capt := ExtractFigureList(child)
				if len(capt) != 0 {
					panic("Sub-caption exists")
				}
				figures = append(figures, list...)
			} else if child.Data == "a" {
				list, capt := ExtractFigure(child)
				if len(capt) != 0 {
					panic("Sub-caption exists")
				}
				figures = append(figures, list...)
			} else {
				panic(fmt.Sprintf("Unhandled tag %v", child.Data))
			}
		}

		return figures, caption
	}

	CleanFigures = func(node *html.Node) []*html.Node {
		toRemove := make([]*html.Node, 0)
		figures := make([]Figure, 0)
		caption := ""

		SetCaption := func(newCaption string) bool {
			if len(newCaption) != 0 {
				if len(caption) == 0 {
					caption = newCaption
				} else {
					return false
					//panic("Caption already sets")
				}
			}
			return true
		}

		for child := node.FirstChild; child != nil; child = child.NextSibling {
			if IsFigure(child) {
				list, capt := ExtractFigure(child)
				/*
					for i, img := range list {
						if len(img.caption) == 0 {
							img.caption = caption
							list[i] = img
							break
						}
					}
				*/
				if len(list) != 0 {
					figures = append(figures, list...)
					if !SetCaption(capt) {
						figures[len(figures)-1].caption = capt
					}
					toRemove = append(toRemove, child)
				}
			} else if child.Data == "ul" || child.Data == "ol" {
				list, capt := ExtractFigureList(child)
				if len(list) != 0 {
					figures = append(figures, list...)
					if !SetCaption(capt) {
						figures[len(figures)-1].caption = capt
					}
					toRemove = append(toRemove, child)
				}
			} else {
				fmt.Println("--", child.Data)

				if len(figures) != 0 {
					node.InsertBefore(CreateGallery(figures, caption), child)
					caption = ""
					figures = nil
				}
			}
		}

		return toRemove
	}

	// Remove empty nodes
	for _, node := range selec.Find("body").Nodes {
		toRemove := make([]*html.Node, 0)

		for child := node.FirstChild; child != nil; child = child.NextSibling {
			if child.Type == html.TextNode {
				data := strings.TrimSpace(child.Data)
				if len(data) == 0 {
					toRemove = append(toRemove, child)
				} else {
					child.Data = "\n" + data + "\n"
				}
			} else if child.Type == html.CommentNode {
				toRemove = append(toRemove, child)
			}
		}

		for _, child := range toRemove {
			node.RemoveChild(child)
		}
	}

	// Move sibling a>img nodes to gallery
	for _, node := range selec.Find("body").Nodes {
		toRemove := CleanSiblingLinks(node)
		for _, child := range toRemove {
			node.RemoveChild(child)
		}
	}

	// Transform figure(s) to gallery
	for _, node := range selec.Find("body").Nodes {
		toRemove := CleanFigures(node)
		for _, child := range toRemove {
			node.RemoveChild(child)
		}
	}

	//goquery.Render(os.Stdout, selec)
}

func (post *Post) Export(output string, dataDir string, blog *Blog) {
	// create file
	dirTitle := fmt.Sprintf("%v-%v", post.date.Format("2006-01-02"), slugify.Slugify(post.title))
	dir := output + "/" + dirTitle
	os.Mkdir(dir, 0755)
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		log.Fatal("Output directory does not exist and cannot be created.")
	}

	imageDir := dir + "/images"
	os.Mkdir(imageDir, 0755)
	if _, err := os.Stat(imageDir); os.IsNotExist(err) {
		log.Fatal("Image directory does not exist and cannot be created.")
	}

	filename := fmt.Sprintf("%v/index.md", dir)
	fmt.Println("++", filename)
	f, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	// remember to close the file
	defer f.Close()

	coverImage := post.coverImage
	if val, ok := blog.attachs[coverImage]; ok {
		coverImage = val.realUrl
	} else {
		panic(fmt.Sprintf("Featured image not found in blog: %v", post.title))
	}
	coverImage = "images/" + blog.AddDownload(coverImage, imageDir)

	f.WriteString("---\n")
	f.WriteString("title: \"" + post.title + "\"\n")
	f.WriteString("date: \"" + post.date.Format(time.RFC3339) + "\"\n")
	f.WriteString("categories: [ \"" + strings.Join(post.categories, "\", \"") + "\" ]\n")
	f.WriteString("tags: [ \"" + strings.Join(post.tags, "\", \"") + "\" ]\n")
	f.WriteString("featured_image: \"" + coverImage + "\"\n")
	f.WriteString("---\n\n")

	converter := md.NewConverter("", true, nil)
	converter.AddRules(
		md.Rule{
			Filter: []string{"gallery"},
			Replacement: func(content string, selec *goquery.Selection, opt *md.Options) *string {
				if len(selec.Nodes) != 1 {
					panic("")
				}
				node := selec.Nodes[0]
				href := ""
				caption := ""
				for _, att := range node.Attr {
					if att.Key == "src" {
						href = att.Val
					} else if att.Key == "caption" {
						caption = att.Val
					}
				}
				fmt.Println(href)
				sources := strings.Split(href, ImgSeparator)
				for i, src := range sources {
					img := strings.Split(src, AltSeparator)
					link := ""
					if strings.HasPrefix(img[0], "http") {
						link = img[0]
					} else if val, ok := blog.attachs[img[0]]; ok {
						link = val.realUrl
					} else {
						panic(fmt.Sprintf("Attachment not found in blog: %v", src))
					}

					sources[i] = "images/" + blog.AddDownload(link, imageDir)
					if len(img) == 2 {
						sources[i] += AltSeparator + img[1]
					}
				}
				return md.String("\n{{< gallery caption=\"" + html.EscapeString(caption) + "\" src=\"" + strings.Join(sources, ImgSeparator) + "\" >}}\n")
			},
		},
	)

	converter.Before(func(selec *goquery.Selection) {
		CleanHtml(blog, imageDir, selec)
	})
	markdown, err := converter.ConvertString(post.content)
	if err != nil {
		log.Fatal(err)
	}
	f.WriteString(markdown)

	if len(post.comments) != 0 {
		fmt.Println("Export comments")
		dataDir = dataDir + "/" + dirTitle
		os.Mkdir(dataDir, 0755)
		if _, err := os.Stat(dataDir); os.IsNotExist(err) {
			log.Fatal("Data directory does not exist and cannot be created.")
		}

		for _, comment := range post.comments {
			comment.Export(dataDir)
		}
	}
}

type Download struct {
	uri      string
	filename string
}

type Attachment struct {
	url     string
	post_id string
	realUrl string
}

type Blog struct {
	posts     []Post
	attachs   map[string]Attachment
	downloads []Download
}

func NewBlog(input string, filterTitle string) *Blog {
	doc := etree.NewDocument()
	if err := doc.ReadFromFile(input); err != nil {
		panic(err)
	}

	blog := Blog{
		posts:     make([]Post, 0),
		attachs:   make(map[string]Attachment),
		downloads: make([]Download, 0),
	}

	channel := doc.SelectElement("rss").SelectElement("channel")
	for _, item := range channel.SelectElements("item") {
		status := item.SelectElement("status").Text()
		if status != "publish" && status != "inherit" {
			continue
		}

		postType := item.SelectElement("post_type").Text()
		if postType == "attachment" {
			blog.attachs[item.SelectElement("post_id").Text()] = Attachment{
				url:     item.SelectElement("attachment_url").Text(),
				post_id: item.SelectElement("post_parent").Text(),
				realUrl: item.SelectElement("guid").Text(),
			}
		}

		if postType != "post" {
			continue
		}

		if filterTitle != "" && !strings.Contains(item.SelectElement("title").Text(), filterTitle) {
			continue
		}

		pubDate := item.SelectElement("pubDate").Text()
		if pubDate == "" {
			panic(fmt.Sprintf("pubDate invalid for post %v", item.SelectElement("post_id").Text()))
		}
		t, err := time.Parse(time.RFC1123, pubDate)
		if err != nil {
			log.Fatal("Error while parsing date :", err)
		}
		loc, err := time.LoadLocation("Europe/Paris")
		t = t.In(loc)

		image := ""
		for _, meta := range item.SelectElements("postmeta") {
			if meta.SelectElement("meta_key").Text() == "_thumbnail_id" {
				image = meta.SelectElement("meta_value").Text()
				break
			}
		}

		categories := make([]string, 0)
		tags := make([]string, 0)
		for _, cat := range item.SelectElements("category") {
			for _, attr := range cat.Attr {
				if attr.Key == "domain" {
					if attr.Value == "category" {
						categories = append(categories, cat.Text())
					} else if attr.Value == "post_tag" {
						tags = append(tags, cat.Text())
					} else {
						panic("Unknown category attribute")
					}
				}
			}
		}
		post := Post{
			title:      item.SelectElement("title").Text(),
			link:       item.SelectElement("link").Text(),
			date:       t,
			content:    item.SelectElement("encoded").Text(),
			coverImage: image,
			categories: categories,
			tags:       tags,
		}

		comments_id := make(map[string]string)
		comments_id["0"] = "0"
		for _, comment := range item.SelectElements("comment") {
			t, err := time.Parse("2006-01-02 15:04:05", comment.SelectElement("comment_date_gmt").Text())
			if err != nil {
				log.Fatal("Error while parsing date :", err)
			}
			t = t.UTC()

			idText := comment.SelectElement("comment_id").Text()
			id, _ := strconv.Atoi(idText)
			rnd := rand.New(rand.NewSource(int64(id)))
			uuid.SetRand(rnd)
			uuid := uuid.New()
			comments_id[idText] = uuid.String()
			comm := Comment{
				id:      uuid.String(),
				name:    comment.SelectElement("comment_author").Text(),
				email:   comment.SelectElement("comment_author_email").Text(),
				date:    t,
				content: comment.SelectElement("comment_content").Text(),
				parent:  comment.SelectElement("comment_parent").Text(),
			}
			post.comments = append(post.comments, comm)
		}
		for i, comment := range post.comments {
			post.comments[i].parent = comments_id[comment.parent]
		}

		blog.posts = append(blog.posts, post)
	}

	return &blog
}

func (blog *Blog) Export(outputDir string, commentsDir string) {
	for _, p := range blog.posts {
		fmt.Println(" -", p.title, "/", len(p.comments))
		p.Export(outputDir, commentsDir, blog)
	}
}

func (blog *Blog) AddDownload(uri string, dir string) string {
	basename := filepath.Base(uri)
	filename := dir + "/" + basename
	blog.downloads = append(blog.downloads, Download{uri, filename})
	return basename
}

func (blog *Blog) downloadFile(download Download) bool {
	if stat, err := os.Stat(download.filename); err == nil && stat.Size() > 0 {
		return false
	}

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}

	resp, err := client.Get(download.uri)
	if err != nil {
		panic(err)
	}
	if resp.StatusCode != 200 {
		panic("StatusCode != 200 for " + download.uri)
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	if err := os.WriteFile(download.filename, data, 0644); err != nil {
		panic(err)
	}

	return true
}

func (blog *Blog) DownloadFiles(scheme string, host string) {
	for _, d := range blog.downloads {
		if scheme != "" || host != "" {
			parsed, err := url.Parse(d.uri)
			if err != nil {
				panic(err)
			}
			if scheme != "" {
				parsed.Scheme = scheme
			}
			if host != "" {
				parsed.Host = host
			}
			d.uri = parsed.String()
		}

		fmt.Print("downloading ", d.filename, " ...")
		if blog.downloadFile(d) {
			fmt.Println("  [done]")
		} else {
			fmt.Println("  [exists]")
		}
	}
}

func main() {
	filename := flag.String("xml-file", "", "export from Worpress blog")
	outputDir := flag.String("output-dir", "post", "where to export yaml")
	commentsDir := flag.String("comments-dir", "comments", "where to export data")
	filterTitle := flag.String("filter-title", "", "set if you want to export by selecting one or more titles")
	host := flag.String("host", "", "set if you want to replace host in download url")
	scheme := flag.String("scheme", "", "set if you want to replace scheme in download url")
	flag.Parse()

	if len(*filename) == 0 {
		log.Fatal("Path to XML file must be set.")
	}

	os.MkdirAll(*outputDir, 0755)
	if _, err := os.Stat(*outputDir); os.IsNotExist(err) {
		log.Fatal("Output directory does not exist and cannot be created.")
	}

	os.MkdirAll(*commentsDir, 0755)
	if _, err := os.Stat(*commentsDir); os.IsNotExist(err) {
		log.Fatal("Comments directory does not exist and cannot be created.")
	}

	blog := NewBlog(*filename, *filterTitle)
	blog.Export(*outputDir, *commentsDir)
	fmt.Println("downloads: ", len(blog.downloads))
	blog.DownloadFiles(*scheme, *host)
}
