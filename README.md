# wp2hugo

## About

Convert an export from WordPress (XML file) to Hugo.

- Export blog posts
- Export comments to individual files
- Download all linked images in content and the featured one
- Convert `a > img` to `img` referencing the highest quality image
- Transform all `img` to an hugo shortcode (to easily create composition)

## Installation

```bash
go get -u gitlab.com/RomainGiraud/wp2hugo
```

## Usage

```bash
wp2hugo -xml-file myBlog.xml -host newurl.com -scheme https
```

Sibling images are transform in a [https://gohugo.io/content-management/shortcodes/](Hugo shortcode).

```jinja
{{< grid-images caption="Common caption" width="3" src="images/img_01.jpg|title 01;images/img_02.jpg|title 02;images/img_03.jpg" >}}
```

Example:

```jinja
{{/*
How to use:
{{< grid-images src="path1|title;path2;path3|title" caption="Main" >}}
*/}}

{{ $mainCaption := (.Get "caption") }}

<figure>
    <div class="gallery">
        {{ $resize := printf "x%v" $.Site.Params.thumbnail.height }}
        {{ if eq (len $data) 1 }}
            {{ $resize = printf "%vx" $.Site.Params.thumbnail.widthSingle }}
        {{ end }}
        {{ range $data }}
            {{ $resource := split . "|" }}
            {{ $link := index $resource 0 }}
            {{ $image := $.Page.Resources.GetMatch $link }}
            {{ $title := index $resource 1 }}
            {{ $thumbnail := $image.Resize $resize }}
            <a href="{{ $image.RelPermalink }}" class="swipebox" title="{{ $resize }}">
                <img src="{{ $thumbnail.RelPermalink }}" alt="{{ $title }}" class="transition ease-in-out duration-300 hover:scale-110"/>
            </a>
        {{ end }}
    </div>
    {{ with $mainCaption }}
    <figcaption class="text-gray-700 text-center">{{ . }}</figcaption>
    {{ end }}
</figure>
```

## Motivation and limitations

I created this tool to export my two personnal blog from WordPress to Hugo.

I tried several soft but ended to create my own for some specific needs.

I do not think it will be updated cause I will not use it anymore. But feel free to use
it, alter it and share it!
