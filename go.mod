module wp2hugo

go 1.18

require (
	github.com/JohannesKaufmann/html-to-markdown v1.3.4
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/beevik/etree v1.1.0
	github.com/google/uuid v1.3.0
	github.com/mozillazg/go-slugify v0.2.0
	golang.org/x/net v0.0.0-20211015210444-4f30a5c0130f
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/mozillazg/go-unidecode v0.1.1 // indirect
	github.com/yuin/goldmark v1.4.1 // indirect
)
